module.exports = {
    user1: {
        id: '12345',
        name: 'John',
        password: 'password',
        channels: ['general', 'hr', 'tech']
    },
    user2: {
        id: '12345',
        name: 'Tom',
        password: 'password',
        channels: ['general', 'tech']
    }
}