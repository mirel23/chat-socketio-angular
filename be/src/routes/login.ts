import {Router} from 'express';

export class LoginRouter {
    private router: Router;

    public constructor() {
        this.router = Router();
        this.init();
    }

    public getRouter() {
        return this.router;
    }

    private init(): void {
        this.router.get('/', (req, res) => {
            res.render('login');
        });

        this.router.post('/', (req, res) => {
            res.json({
                success: true,
                message: 'Index page'
            })
        });
    }
}
