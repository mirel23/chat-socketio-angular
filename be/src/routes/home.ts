import {Router} from 'express';

export class HomeRouter {
    private router: Router;

    public constructor() {
        this.router = Router();
        this.init();
    }

    public getRouter() {
        return this.router;
    }

    private init(): void {
        this.router.get('/', (req, res) => {
            res.render('index');
        });
    }
}
