import * as express from 'express';
import {HomeRouter} from './home';
import {LoginRouter} from './login';

export const register = ( app: express.Application ) => {
    let homeRouter = new HomeRouter();
    let loginRouter = new LoginRouter();

    app.use('/', homeRouter.getRouter());
    app.use('/login', loginRouter.getRouter());
};
