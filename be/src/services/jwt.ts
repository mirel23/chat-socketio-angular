import * as jwt from 'jsonwebtoken';
import {SignOptions} from 'jsonwebtoken';
import {VerifyOptions} from 'jsonwebtoken';

const JWT_KEY = process.env.JWT_SECRET_KEY;

const JWTService = {
    sign: (payload: string | Buffer | object) => {
        var signOptions: SignOptions = {
            expiresIn:  '24h',
            algorithm:  'RS256'
        };
        return jwt.sign(payload, JWT_KEY, signOptions);
    },
    verify: (token: string) => {
        var verifyOptions: VerifyOptions = {
            maxAge:  '24h',
            algorithms:  ['RS256']
        };

        try {
            return jwt.verify(token, JWT_KEY, verifyOptions);
        } catch (err){
            return false;
        }
    },
    decode: (token: string) => {
        return jwt.decode(token, {complete: true});
    }
};

export default JWTService;