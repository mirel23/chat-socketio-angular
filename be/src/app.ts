import dotenv from 'dotenv';
import http from 'http';
import express from 'express';
import path from 'path';
import * as routes from './routes';
import * as socketIO from 'socket.io';
import {Message} from '../../shared/models/message';

// Initialize configuration
dotenv.config();

const port = process.env.SERVER_PORT;
const serverUrl = process.env.SERVER_URL;

// Create a new express application instance
const app: express.Application = express();
const server = http.createServer(app);
// Bind socket.io
const io = socketIO.listen(server);

// Configure Express to use EJS
app.set( 'views', path.join( __dirname, 'views' ) );
app.set( 'view engine', 'pug' );

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Configure routes
routes.register( app );

io.on('connection', (socket) => {
    console.log('user connected');

    socket.on('message', (message: Message) => {
        console.log(message);
        // Broadcast to other users
        io.emit('message', message);
    });
});

// Start the express server
server.listen( port, () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at ${ serverUrl }` );
} );

module.exports = app;
