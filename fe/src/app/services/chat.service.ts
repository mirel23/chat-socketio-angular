import * as socketIo from 'socket.io-client';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Message} from '../../../../shared/models/message';

@Injectable()
export class ChatService {
  private url = environment.serverUrl;
  private socket;

  constructor() {
    this.socket = socketIo(this.url);
  }

  public sendMessage(message: Message) {
      this.socket.emit('message', message);
  }

  public onMessage(): Observable<Message> {
    return Observable.create(observer => {
      this.socket.on('message', (data: Message) => {
        observer.next(data);
      });
    });
  }
}
