import { Component } from '@angular/core';
import {ChatService} from './services/chat.service';
import {Message} from '../../../shared/models/message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  userMessage: '';
  messages: string[] = [];

  constructor(private chatService: ChatService) {}

  sendMessage() {
    // TODO get User from LocalStorage
    this.chatService.sendMessage({
      from: {
        id: '1234',
        name: 'John'
      },
      content: this.userMessage
    });
    this.userMessage = '';
  }

  ngOnInit() {
    this.chatService
      .onMessage()
      .subscribe((message: Message) => {
        this.messages.push(message.content);
      });
  }
}
